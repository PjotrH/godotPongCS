using Godot;
using System;

public class GameMain : Node2D
{
    // Member variables here, example:
    // private int a = 2;
    // private string b = "textvar";
	private float screenHeight;
    private float screenWidth;
    private Vector2 padSize;
	private Vector2 direction = new Vector2(1.0f, 0.0f);	
    const float initialBallSpeed = 80;
    float ballSpeed = initialBallSpeed;
    const int padSpeed = 150;

    public override void _Ready()
    {
        Vector2 viewPort = GetViewport().Size;
        screenWidth = viewPort.x;
        screenHeight = viewPort.y;

        //screenHeight = GetViewport().Size[0];
        //screenWidth = GetViewport().Size[1];

        Sprite node = (Sprite)GetNode("Right");
        Texture tex = node.GetTexture();
        var size = tex.GetSize();
        padSize = size;
        
        SetProcess(true);

        // Called every time the node is added to the scene.
        // Initialization here
    }

   public override void _Process(float delta)
   {
       Vector2 ballPos = adjustBall(delta);
       playerInput(delta);
       adjustPad(ballPos);
       //adjustPads((Sprite)GetNode("Left"), ballPos);
       //adjustPads((Sprite)GetNode("Right"), ballPos);
       // Called every frame. Delta is time since last frame.
       // Update game logic here.
   }

   private void adjustPad(Vector2 ballPos){
       Random random = new Random();

       Sprite leftSprite = (Sprite)GetNode("Left"); //grab left node as sprite
       Vector2 leftPos = leftSprite.GetPosition(); //grab xy position of left sprite
       Vector2 halfLeftPos = leftPos - padSize * 0.5f; //subtract half the width
       Rect2 leftRect = new Rect2(halfLeftPos, padSize); //put collision rectangle in centre (i.e. half width)

       Sprite rightSprite = (Sprite)GetNode("Right");
       Vector2 rightPos = rightSprite.GetPosition();
       Vector2 halfRightPos = rightPos - padSize * 0.5f;
       Rect2 rightRect = new Rect2(halfRightPos, padSize);

        if( leftRect.HasPoint(ballPos) & direction.x < 0 | rightRect.HasPoint(ballPos) & direction.x > 0){
            direction.x = -direction.x; //if the rectangle of a pad touches the xy position of the and the width direction is greater than 0, invert direction
            direction.y = (float)random.NextDouble() * 2.0f -1.0f; //generates a double between 0 and 1, casts to float, sets as new direction of ball
            direction = direction.Normalized(); //not really sure what this does but I'm guessing it makes sure the ball can only go a random direction in the front arc of the pad it hit
            ballSpeed = ballSpeed * 1.1f; //increase speed by 10% with each ball hit
        };
   }

    //same as adjustPad but can pass any pad instead of hardcoding specific pad
    //at least that was the idea but it doesn't work - it doesn't seem to recognise the sprite path string at line 78
    //Ignore this method unless you can fix it
    private void adjustPads(Sprite pad, Vector2 ballPos){
        Sprite sprite = pad;
        Vector2 spritePos = sprite.GetPosition();
        Vector2 halfPos = spritePos - padSize * 0.5f;
        Rect2 rect = new Rect2(halfPos, padSize);
        Node spriteNode = (Node)sprite;
        
        if( spriteNode.GetPath() == "Left" & rect.HasPoint(ballPos) & direction.x < 0 | spriteNode.GetPath() == "Right" & rect.HasPoint(ballPos) & direction.x > 0){
            direction.x = -direction.x; 
            Random random = new Random();
            direction.y = (float)random.NextDouble() * 2.0f -1.0f; 
            direction = direction.Normalized(); 
            ballSpeed = ballSpeed * 1.1f; 
        };
   }

   private Vector2 adjustBall(float delta){
       Node ballNode = GetNode("Ball");
       //gets ball node as sprite (cast as sprite because node doesn't have a getposition method)
       Sprite ballSprite = (Sprite)ballNode;
       //gets x,y position of ball sprite as vector
       Vector2 ballPos = ballSprite.GetPosition();
       //adjusts position of ball by direction, speed, and time (gotta go fast)
       ballPos += direction * ballSpeed * delta;
       //if the height position of the ball is less than 0 (bottom of screen), and the height direction is less than 0 (going DOWNWARD on y axis) 
       //OR
       //if the height position of the ball is greater than the height of the screen (top of screen) and the direction is *greater* than 0(going UPWARD on y axis)
       if(ballPos.y < 0 & direction.y <0 || ballPos.y > screenHeight & direction.y > 0){
           direction.y = -direction.y; //invert y direction
       };

       if(ballPos.x > screenWidth || ballPos.x < 0){
           ballPos.x = screenWidth * 0.5f;
           ballPos.y = screenHeight * 0.5f;
           ballSpeed = initialBallSpeed;
           direction = new Vector2(-1.0f, 0.0f); //invert direction in which the ball starts going
       };

       ballSprite.SetPosition(ballPos);

       return ballPos;
   }

   private void playerInput(float delta){
       Node leftNode = GetNode("Left");
       Sprite leftPad = (Sprite)leftNode;
       Vector2 leftPos = leftPad.GetPosition();

       if(leftPos.y > 0 & Input.IsActionPressed("left_move_up")){
           leftPos.y = leftPos.y + -padSpeed * delta;
       };
       if(leftPos.y < screenHeight & Input.IsActionPressed("left_move_down")){
           leftPos.y = leftPos.y + padSpeed * delta;
       }

       leftPad.SetPosition(leftPos);

       Node rightNode = GetNode("Right");
       Sprite rightPad = (Sprite)rightNode;
       Vector2 rightPos = rightPad.GetPosition();

       if(rightPos.y > 0 & Input.IsActionPressed("right_move_up")){
           rightPos.y = rightPos.y + -padSpeed * delta;
       };
       if(rightPos.y < screenHeight & Input.IsActionPressed("right_move_down")){
           rightPos.y = rightPos.y + padSpeed * delta;
       }

       rightPad.SetPosition(rightPos);
       
    }
}
